import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class StudentsNewImplService extends StudentService{

  constructor(private http: HttpClient) { 
    super();
  }
  getStudents(): Observable<Student[]>{
    return this.http.get<Student[]>('assets/newstudent.json');
  }

  students: Student[] = [
		{
			id: 1,
			studentId:"SE-001",
			name:"Prayuth",
			surname:"The minister",
			gpa:3.59,
			image:"https://se331.s3-ap-southeast-1.amazonaws.com/images/tu.jpg",
			featured:false,
			penAmount:15,
      description:"The great man ever!!!!"
		},
		{
			id: 2,
			studentId:"SE-002",
			name:"Jurgen",
			surname:"Kloop",
			gpa:2.15,
		  image:"https://se331.s3-ap-southeast-1.amazonaws.com/images/klopp.jpg",
			featured:true,
			penAmount:2,
      description:"The man for the Kop"
		},
		{
			id: 3,
			studentId:"SE-003",
			name:"Ae+",
			surname:"Parena",
			gpa:2.15,
			image:"https://se331.s3-ap-southeast-1.amazonaws.com/images/pareena.jpg",
			featured:true,
			penAmount:0,
      description : "The most beloved one"
		}
];

}
