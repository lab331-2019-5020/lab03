import { TestBed } from '@angular/core/testing';

import { StudentsNewImplService } from './students-new-impl.service';

describe('StudentsNewImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentsNewImplService = TestBed.get(StudentsNewImplService);
    expect(service).toBeTruthy();
  });
});
