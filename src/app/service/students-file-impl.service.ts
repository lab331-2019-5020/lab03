import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../entity/student';

@Injectable({
  providedIn: 'root'
})
export class StudentsFileImplService extends StudentService{

  constructor(private http: HttpClient) { 
    super();
  }
  getStudents(): Observable<Student[]>{
    return this.http.get<Student[]>('assets/student.json');
  }

  students: Student[] = [{
    id: 1,
    studentId: '562110507',
    name: 'Prayuth',
    surname: 'Tu',
    gpa: 4.00,
    image: 'assets/images/tu.jpg',
    featured: true,
    penAmount: 1,
    description:"one"
  }, {
    id: 2,
    studentId: '562110509',
    name: 'Pu',
    surname: 'Priya',
    gpa: 0.28,
    image: '/assets/images/pu.jpg', 
    featured: false,
    penAmount: 0,
    description:"two"
  }];

}
